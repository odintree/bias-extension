import React from 'react';
import './index.css';
import App from './App';
import Headers from './Header'
import Nav from  './Nav'
import Point from './Point'
import './style/Main.css';


class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: 'Images',
            analyze: [],
        };
    }

    handleClick = (button) => {
        this.setState((state, props) => {
            return {display: button};
        })

    }
    analyzeClick = (result) => {
        this.setState((state, props) =>{
            console.log('result[0] aka fact',result)
            return {analyze: result };
        })

    }
    render() {
        return (
            <div className="Main">
                <Headers/>
                <Nav clickHandler={this.handleClick} /> 
                <App value={this.state.display}/> 
                <Point 
                    value={this.state.display}
                    political={this.state.analyze[0] || []}
                    fact={this.state.analyze[1] || []}
                    subj = {this.state.analyze[2] || ''}
                    clickHandler={this.analyzeClick}
                />
            </div>
        )
    }
}

export default Main;
