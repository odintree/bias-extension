/*global chrome*/

import React from 'react';
import './style/Point.css';
import axios from "axios"


/* const analyze = () => {
    console.log('areee you working')
    chrome.tabs.query({
        active: true,
        currentWindow: true,
    }, (tabs) => {
        // Send message to script file
        chrome.tabs.sendMessage(
        tabs[0].id,
        { analyze: true, data: 'text'},
        );
    });
} */

let result = ''





const Point = (props) => {
    console.log('props.fact[0]', props.fact)
    console.log('props.pol', props.political)
    let real = Math.floor(props.fact[1]*100) 
    let fake = Math.ceil(props.fact[0]*100) 
    let left = Math.floor(props.political[0]*100) 
    let right = Math.ceil(props.political[1]*100)
    let subj = props.subj
    console.log('r',real, 'f', fake,'l' ,left , 'ri',right) 

    let displayReal = ""
    let displayFake = "" 
    let displayLeft = ""
    let displayRight = ""
    let displaySubj
    if (props.fact.length>0) {
        displayReal = real + " %"
        displayFake = fake + " %"
    }

    if (props.political.length>0) {
        displayLeft = left + " %"
        displayRight = right + " %"
    } 
    
    if (subj) displaySubj = "Subjectivity: " +  Math.round(subj*100)/100 + " %"

    const checkWebsite = () => {
        chrome.tabs.query({
            active: true, 
            currentWindow: true}
        , (tabs) => {
            let url = tabs[0].url;
            console.log('url', url)
            if(url.includes("https://www.buzzfeed.com") || url.includes("https://www.theguardian.com") || url.includes("https://www.nytimes.com")
            || url.includes("https://www.reuters.com") || url.includes("https://www.foxnews.com") || url.includes("http://www.foxnews.com") || url.includes("https://nypost.com")
            || url.includes("https://www.nationalreview.com") || url.includes("https://talkingpointsmemo.com") || url.includes("https://www.theatlantic.com")
            || url.includes("https://www.vox.com") || url.includes("https://www.breitbart.com") || url.includes("http://www.breitbart.com")) {
                console.log('I AM HERE')
                console.log('urlNEWSS', url)
                axios.get('http://127.0.0.1:5000/api?url='
                    +url)
               .then(res => {
                    console.log('res',res)
                    console.log('fact: ', res.data.factCheckPred, 'political: ', res.data.politicalInclinationPred )
                    result = res.data.factCheckPred.concat(res.data.politicalInclinationPred,res.data.subjectivity_score)
                    props.clickHandler(result) 
               })
               .catch( error => {
                    console.log('error',error);
                })
            }
        })


    }
  
    console.log('props', props)
    if (props.value === "Analyze") {
        return (
            <div> 
                <form target="_blank" id="analyze-form" action="localhost:5000" method="GET">
                    <input id="url" name="url" type="hidden"/>
                </form>
                <div className="Point"> 
                    <div className="Point-left"> REAL {displayReal} </div>
                    <div className="Point-right"> FAKE {displayFake} </div>    
                </div>
                <div className="Point"> 
                    <div className="Point-left"> LEFT  {displayLeft} </div>
                    <div className="Point-right"> RIGHT {displayRight} </div>    
                </div>
                <div className="Point-subj"> {displaySubj} </div>
                <div className="Point-button-div"> 
                    <button className="Point-button" onClick={checkWebsite}> ANALYZE </button>     
                </div>
            </div>
        )
    } else return null
}

export default Point;
