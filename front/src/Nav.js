import React from 'react';
import './style/Nav.css';


const options = [
  {
    text:'Images',
    class: 'Nav-left'
  },
  {
    text: 'Analyze',
    class: 'Nav-right'

  }
]

const Nav = (props) => {
    console.log('props123', props.clickHandler)
    const ImagesClick = (event) => {
      event.preventDefault();
      props.clickHandler('Images')
    }
    const AnalyzeClick = (event) => {
      event.preventDefault();
      props.clickHandler('Analyze')
    }


     

    return (
      <div  className="Nav">
        <div  className="Nav-left" onClick={ImagesClick}><a className="optionText" href="">Images</a> </div>
        <div  className="Nav-right" onClick={AnalyzeClick}><a className="optionText" href="">Analyze</a> </div>
      </div>
    )
}
export default Nav;
