import React from 'react';
import './style/Header.css';
import logo from './echo.png';
import login from './dbowner.png'




const Headers = (props) => {
  const text = 'Every voice matters'
    return (
      <div className="Header">
        <div className="inline"> <img src={logo} className="Header-logo"/></div>
        <div className="inline text"> {text} </div>
        <div className="inline-right"> <img src={login} className="Header-login"/> </div> 
      </div>
    )
}


export default Headers 