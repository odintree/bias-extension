/*global chrome*/

// IMPORTS
import React from 'react';
import './style/App.css';
import logo from './echo.png';
import loop from './search-256.png'




console.log('window.navigator.language',window.navigator.language);

//CONST VALUES
const key = 'trnsl.1.1.20180609T133438Z.3de2ba4b9a59d9f2.b52473da05f7211d7fb4f21c225045e950419bdd';

const allLangCodes = ["af", "am", "ar", "az", "ba", "be", "bg", "bn", "bs", "ca", "ceb", "cs", "cy", "da", "de", "el", "en", "eo", "es", "et", "eu", "fa", "fi", "fr", "ga", "gd", "gl", "gu", "he", "hi", "hr", "ht", "hu", "hy", "id", "is", "it", "ja", "jv", "ka", "kk", "km", "kn", "ko", "ky", "la", "lb", "lo", "lt", "lv", "mg", "mhr", "mi", "mk", "ml", "mn", "mr", "mrj", "ms", "mt", "my", "ne", "nl", "no", "pa", "pap", "pl", "pt", "ro", "ru", "si", "sk", "sl", "sq", "sr", "su", "sv", "sw", "ta", "te", "tg", "th", "tl", "tr", "tt", "udm", "uk", "ur", "uz", "vi", "xh", "yi", "zh"]

const spanishM = ['médico', 'ingeniero', 'cirujano', 'maestro', 'arquitecto', 'arqueólogo', 'abogado', 'peluquero', 'biólogo', 'carnicero', 'carpintero', 'payaso', 'farmacéutico', 'químico', 'bombero', 'jardinero', 'geólogo', 'ginecólogo', 'secretario', 'joyero', 'bibliotecario', 'cartero', 'mecánico', 'ministro', 'músico', 'enfermero', 'fotógrafo', 'político', 'psicólogo', 'científico', 'camarero', 'cocinero']
const spanishW = ['médica', 'ingeniera', 'cirujana', 'médica', 'ingeniera', 'cirujana', 'maestra', 'arquitecta', 'arqueóloga', 'abogada', 'peluquera', 'bióloga', 'carnicera', 'carpintera', 'payasa', 'farmacéutica', 'química', 'bombera', 'jardinera', 'geóloga', 'ginecóloga', 'secretaria', 'joyera', 'bibliotecaria', 'cartera', 'mecánica', 'ministra', 'música', 'enfermera', 'fotógrafa', 'política', 'psicóloga', 'científica', 'camarera', 'cocinera']
const browserLang = window.navigator.language.includes('en') ? 'en': window.navigator.language 
const defaultLanguages = ['es', 'zh']
const languages = (!defaultLanguages.includes(browserLang)&&allLangCodes.includes(browserLang)) ? defaultLanguages.concat(browserLang) : defaultLanguages
const original = 'en'

const test = () => {
  fetch('https://translate.yandex.net/api/v1.5/tr.json/getLangs?key='
      +key+'&ui=en', {
      method: 'POST',
      }).then((res) => res.json())
      .then((data) => console.log('data',data))
      .catch((err)=>console.log(err))
}
test()  

//CONST FUNCTIONS
const sendTranslation = (text) => {
  // Get active tab
  chrome.tabs.query({
    active: true,
    currentWindow: true,
  }, (tabs) => {
    // Send message to script file
    chrome.tabs.sendMessage(
      tabs[0].id,
      { injectApp: true, data: text},
    );
  });
}


const setTranslation = (isDG,text,array) => {
  console.log('isDG')
  if (isDG) {
    let text2 = ''
    if (text.endsWith('o')) text2 = text.slice(0,-1)+'a'
    if (text.endsWith('a')) text2 = text.slice(0,-1)+'o'
    array.unshift(text,text2)
  } else {
    array.unshift(text)
  }
  console.log('ARRAY', array)
}

const startSearch = (event) => {
  var translationText = [this.searchText.value]
  event.preventDefault();
  console.log('DATA: ', this.searchText.value)
  let numberOfReplies = 0 
  let translationAmount = languages.length 
  languages.map((language) => {
    fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key='
      +key+'&lang='+original+'-'
      +language
      +'&text='      
      +this.searchText.value+'', {
      method: 'POST',
      }).then((res) => res.json())
      .then((data) => {
        if (language!==original) {
          let isDG = false
          if (language === 'es') {
            if (spanishM.includes(data.text[0])) isDG = true
            if (spanishW.includes(data.text[0])) isDG = true  
          }
          console.log('data',data.text[0],language,isDG)
          setTranslation(isDG,data.text[0],translationText)
          numberOfReplies++
        } else translationAmount-- 
        if (numberOfReplies === translationAmount) {
          let finalString = translationText.join('; ')
          sendTranslation(finalString)
        }
      })
      .catch((err)=>console.log(err))
  })
} 


//RENDER
const App = (props) => {
  const inputStyle = {
    padding:'6px 15px 6px 20px',
    margin:'1px'
  }
  console.log('propsAPP', props)
  const text = 'Every voice matters'

  if (props.value === "Images") {
    return (
      <div className="App">
        <div className=""> <img src={logo} className="App-logo"/></div>
        <div className="App-title"> {text} </div>
        <div><input className="App-input"
          style={inputStyle}
          type="text" 
          ref={input => this.searchText = input}
          >
          </input>
          </div>
        <input className="App-input-button" type="submit" value="Search" onClick={startSearch}></input>
      </div>
    )
  } else return null
}

export default App;









