import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './Main';
import registerServiceWorker from './registerServiceWorker';


class Index extends React.Component {

    state = {

    }

    render() {
        return (
            <Main> 
            </Main>
        )
    }
}

ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorker();
