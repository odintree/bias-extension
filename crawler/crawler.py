import scrapy
import sys
from scrapy.crawler import CrawlerProcess
from multiprocessing import Process, Queue
from twisted.internet import reactor

sys.dont_write_bytecode = True

class spider(scrapy.Spider):
    name = 'Article Crawler'

    allowed_domains = ["www.buzzfeed.com", "www.theguardian.com", "www.nytimes.com", "www.reuters.com", "www.foxnews.com", "nypost.com", "www.nationalreview.com", "talkingpointsmemo.com", "www.theatlantic.com", "www.vox.com", "www.breitbart.com"]

    def __init__(self, url = None):
        self.start_urls = [url]
        super(spider, self).__init__(url)

    def parse(self, response):
        title = ""
        full_text = ""

        if "www.buzzfeed.com" in self.start_urls[0]:
            title = response.css('h1.buzz-title::text')[0].extract()
            for e in response.css('article.buzz.article>div>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '
        
        elif "www.theguardian.com" in self.start_urls[0]:
            title = response.css('h1.content__headline::text')[0].extract()
            for e in response.css('div.content__article-body>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '
        
        elif "www.nytimes.com" in self.start_urls[0]:
            title = response.xpath('//h1/span/text()')[0].extract()
            for e in response.css('article#story>div.StoryBodyCompanionColumn>div>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "edition.cnn.com" in self.start_urls[0]:
            title = response.css('h1.pg-headline::text')[0].extract()
            for e in response.css('div[itemprop="articleBody"] div.zn-body__paragraph'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.reuters.com" in self.start_urls[0]:
            main_article = response.css('div.container_19G5B')[0]
            title = main_article.css('h1.headline_2zdFM::text')[0].extract()
            for e in main_article.css('div.container_385r0>div.left_qEP9d p:not(.content_27_rw):not(.reading-time_39cOG)'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.foxnews.com" in self.start_urls[0]:
            title = response.css('h1.headline::text')[0].extract()
            for e in response.css('div.article-content>div.article-body>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "nypost.com" in self.start_urls[0]:
            title = response.css('div.article-header>h1>a::text')[0].extract()
            for e in response.css('div.article-header>div.entry-content>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.nationalreview.com" in self.start_urls[0]:
            title = response.css('h1.article-header__title::text')[0].extract()
            for e in response.css('div.article-content>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "talkingpointsmemo.com" in self.start_urls[0]:
            title = response.css('div.Article__Head>h1::text')[0].extract()
            for e in response.css('div.Article__Content>div#article-content>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.theatlantic.com" in self.start_urls[0]:
            title = response.css('h1[itemprop="headline"]::text')[0].extract()
            for e in response.css('section[itemprop="articleBody"]>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.vox.com" in self.start_urls[0]:
            title = response.css('h1.c-page-title::text')[0].extract()
            for e in response.css('div.c-entry-content>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        elif "www.breitbart.com" in self.start_urls[0]:
            title = response.css('article.the-article>header>h1::text')[0].extract()
            for e in response.css('div.entry-content>p'):
                text = e.css('::text').extract()
                if len(text) > 0:
                    full_text += ''.join(text).strip() + ' '

        yield { 'title' : title.strip(), 'article' : full_text }

def run(url):
    def f(q):
        try:
            process = CrawlerProcess({
                'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
                'FEED_FORMAT': 'csv',
                'FEED_URI': 'article.csv',
                'FEED_EXPORT_FIELDS': ['title', 'article']
            })
            deferred = process.crawl(spider, url = url)
            deferred.addBoth(lambda _: reactor.stop())
            reactor.run()
            q.put(None)
        except Exception as e:
            q.put(e)

    q = Queue()
    p = Process(target=f, args=(q,))
    p.start()
    result = q.get()
    p.join()

    if result is not None:
        raise result