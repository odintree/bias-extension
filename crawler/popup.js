$(document).ready(function(){
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        var url = tabs[0].url;
        if(url.includes("https://www.buzzfeed.com") || url.includes("https://www.theguardian.com") || url.includes("https://www.nytimes.com")
        || url.includes("https://www.reuters.com") || url.includes("https://www.foxnews.com") || url.includes("https://nypost.com")
        || url.includes("https://www.nationalreview.com") || url.includes("https://talkingpointsmemo.com") || url.includes("https://www.theatlantic.com")
        || url.includes("https://www.vox.com") || url.includes("https://www.breitbart.com")) {
            $( "#analyze" ).show();
            $( "#url" ).val(url);
            $( "#default" ).hide();
        }
        else {
            $( "#analyze" ).hide();
            $( "#default" ).show();
        }
    });
});