from keras.models import model_from_json
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

def predictFactCheck(article):
    json_file = open('FactCheckmodel.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("FactCheckmodelWeights.h5")
    print("Loaded model from disk")

    loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    test_inforwars=article

    tokenizertest = Tokenizer(num_words=10000)
    tokenizertest.fit_on_texts(test_inforwars)
    sequencetest = tokenizertest.texts_to_sequences(test_inforwars)
    word_index2 = tokenizertest.word_index
    print('Found %s unique tokens.' % len(word_index2))

    datatest = pad_sequences(sequencetest, maxlen=1000)
    pred= loaded_model.predict(datatest)

    y_classes = pred.argmax(axis=-1)

    return {
        'factCheckPred': pred.tolist(),
        'factCheckYClasses': y_classes.tolist()
    }

def predictPoliticalInclination(article):
    json_file = open('PoliticalInclination.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("PoliticalInclinations.h5")
    print("Loaded model from disk")

    loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    test_inforwars=article

    tokenizertest = Tokenizer(num_words=10000)
    tokenizertest.fit_on_texts(test_inforwars)
    sequencetest = tokenizertest.texts_to_sequences(test_inforwars)
    word_index2 = tokenizertest.word_index
    print('Found %s unique tokens.' % len(word_index2))

    datatest = pad_sequences(sequencetest, maxlen=1000)
    pred= loaded_model.predict(datatest)

    y_classes = pred.argmax(axis=-1)

    return {
        'politicalInclinationPred': pred.tolist(),
        'politicalInclinationYClasses': y_classes.tolist()
    }
