import os
import logging
import crawler
import nltk
import pandas
import string
import urllib
import sys
import json
import runjson
from textblob import TextBlob
from flask import Flask
from flask import request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

sys.dont_write_bytecode = True
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

@app.route("/api")
def calculate():
    logging.getLogger('scrapy').setLevel(logging.WARNING)
    #insert url here
    url =  urllib.parse.unquote(str(request.args.get('url')))
    crawler.run(url)
    data = pandas.read_csv("article.csv", low_memory=False)
    if data['title'].hasnans or data['article'].hasnans:
        os.remove('article.csv')
        return "There was an error parsing the article."

    translator = str.maketrans("", "", string.punctuation)
    sentences = nltk.sent_tokenize(data['title'].values[0]) + nltk.sent_tokenize(data['article'].values[0])
    bullResult = runjson.predictFactCheck([sentences])
    inclinResult = runjson.predictPoliticalInclination([sentences])
    sentences = [ str(x).translate(translator) for x in enumerate(sentences) ]
    subjective_sentences = 0
    for sentence in sentences:
        if TextBlob(sentence).subjectivity >= .5:
            subjective_sentences += 1

    subjectivity_score = subjective_sentences/len(sentences) * 100

    fullResult = bullResult.copy()
    fullResult.update(inclinResult)
    fullResult['subjectivity_score'] = subjectivity_score
    os.remove('article.csv')
    return json.dumps(fullResult)