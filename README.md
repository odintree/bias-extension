## How to run:
On your terminal go to `crawler` folder and:
- run `source crawler/bin/activate`
- run `FLASK_APP=main.py flask run`


Assuming that you have 'npm'
Inside the folder `front` run:

```
npm install  
npm run build 
```


Google Chrome:  
go to chrome://extensions  
turn on 'Developer Mode'  
click on 'Load Package' and choose 'front/build' folder  
